cat banner.txt
sed -i 's/FromGoal-->/FromGoal/g' pom.xml 
sed -i 's/<!--toGoal/toGoal/g' pom.xml
mvn clean package
cd target/
cp *.jar app.jar
java -jar app.jar
cd ..
sed -i 's/FromGoal/FromGoal-->/g' pom.xml
sed -i 's/toGoal/<!--toGoal/g' pom.xml
