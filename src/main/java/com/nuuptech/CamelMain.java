package com.nuuptech;

import org.apache.camel.cdi.Main;

/**
 * A Camel Application
 */

public class CamelMain {

    /**
     * A main() so we can easily run these routing rules in our IDE
     */
    public static void main(String... args) throws Exception {
        Main main = new Main();
        main.run(args);
    }

}

