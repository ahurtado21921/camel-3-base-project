package com.nuuptech.camel.model;

import javax.inject.Named;

import lombok.Data;

@Data
@Named
public class ObjectExample {

	private String id;
	private String name;
	
}
