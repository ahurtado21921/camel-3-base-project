package com.nuuptech.camel;

import javax.inject.Inject;

import org.apache.camel.Endpoint;
import org.apache.camel.LoggingLevel;
import org.apache.camel.PropertyInject;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.cdi.Uri;
import org.apache.camel.model.dataformat.JsonLibrary;

import com.nuuptech.camel.model.ObjectExample;

import lombok.Value;

/**
 * Configures all our Camel routes, components, endpoints and beans
 */
public class MyRoutes extends RouteBuilder {

    @Inject
    @Uri("timer:exampleProject?period=30s")
    private Endpoint input;
    
    @PropertyInject("enpointExample")
    private String example;
    @Override
    public void configure() {
        // you can configure the route rule with Java DSL here

    	restConfiguration().component("undertow")
        .port("{{port}}");
    	
    	
        rest("/example")
        .get()
            .toD(example);

    	from(example)
    	.setBody()
			.simple("{{sample.body}}")
		.log("hello from Rest");
    	
        from(input)
        	.id("demoRoute")
        	.setBody()
        		.simple("{{sample.body}}")
        	.bean("logCons","addLog")
        	.setBody()
        		.constant(new ObjectExample())
        	.marshal()
        		.json(JsonLibrary.Jackson)
        	.bean("logCons","addLog")
        	.log(LoggingLevel.INFO,"${exchange.getProperty('logOutput')}");
        
    }

}
