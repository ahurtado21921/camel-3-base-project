package com.nuuptech.camel.utils;

import javax.inject.Named;

import org.apache.camel.Body;
import org.apache.camel.Exchange;
import org.apache.camel.ExchangeProperty;

import lombok.Data;

@Data
@Named("logCons")
public class LoggerConstructor {

	public void addLog(@ExchangeProperty("logOutput") StringBuilder outPut,@Body String logCache,Exchange ex) {
		if (outPut == null) {
			outPut = new StringBuilder();
			outPut.append("\n:::::::::::::::::::::MS-DEMO:::::::::::::::::::::\n");
			outPut.append(logCache);
			outPut.append("\n");
		} else {
			outPut.append(logCache);
			outPut.append("\n");
		}
		ex.setProperty("logOutput", outPut);
		
		
	}
}
