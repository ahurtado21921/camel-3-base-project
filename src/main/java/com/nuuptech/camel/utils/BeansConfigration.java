package com.nuuptech.camel.utils;

import java.util.ArrayList;

import java.util.List;

import javax.enterprise.inject.Produces;

import javax.inject.Named;

//import org.apache.camel.component.activemq.ActiveMQComponent;
import org.apache.camel.component.properties.PropertiesComponent;
import org.apache.camel.component.properties.PropertiesLocation;

public class BeansConfigration {

	/**
	 * Create the Camel properties component using CDI @Produces with the name:
	 * properties
	 */
	@Produces
	@Named("properties")
	PropertiesComponent propertiesComponent() {
		PropertiesComponent component = new PropertiesComponent();
		component.setIgnoreMissingLocation(true);
		List<PropertiesLocation> locations = new ArrayList<PropertiesLocation>();
		PropertiesLocation e = new PropertiesLocation("file:/deployments/config/application.properties");
		locations.add(e);
		e = new PropertiesLocation("classpath:application.properties");
		locations.add(e);
		component.setLocations(locations);
		return component;
	}
//
//	@Produces
//	@Named("amq")
//	@ApplicationScoped
//	ActiveMQComponent amq() {
//		ActiveMQComponent amq = new ActiveMQComponent();
//		amq.setBrokerURL("tcp://localhost:61616");
//		amq.setUsername("userJFp");
//		amq.setPassword("jG2yTiMR");
//		return amq;
//	}

}
